# Cookie Saver

Safari version 12.1 and above include the intelligent tracking prevention system (ITP) `2.1`. This version of ITP has a big impact on web analytics, the conversion linker, as well as conversion optimization experiments. The Cookie Saver provides an example implementation of a back end service that can preserve your front end JavaScrip cookies. When requesting the service, all cookies are sent to the service via the HTTP `cookies` header. The service will look for relevant cookies as configured and respond with a HTTP `set-cookie` header in the response to set those cookies again without the ITP 2.1 seven day lifetime.

## Deployment

The cookie saver is written in JavaScript specifically for the [Google App Engine Node.js](https://cloud.google.com/appengine/) environment. This environment allows for very flexible scaling at a very low cost making it easy to deploy the service quickly and independent of an existing web infrastructure.

## Implementation

1. Create a GCP project and GCP billing account
2. Deploy the Cookie Saver
3. Point the subdomain to the backend service by updating the DNS records.
4. Update the tracking code or Google Tag Manager setup to call the backend service before unload of every page:

```html
<script>
  (function() {
    var safari = navigator.userAgent.match(/(Version)\/(\d+)\.(\d+)(?:\.(\d+)|).*Safari\//g);
    var version = safari && safari[0].match(/(\d+)\.(\d+)/g);
    if (version && parseFloat(version[0]) >= 12.1) {
      window.addEventListener("beforeunload", function (event) {
        navigator.sendBeacon("https://cookie.trakken.de");
      });
    }
  }())
</script>
```
