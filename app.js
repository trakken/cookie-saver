const express = require("express");
const cookieParser = require("cookie-parser");
const cors = require("cors");

const app = express();
app.use(cors({ origin: true, credentials: true }));
app.use(cookieParser());

const port = 8080;

const cookieProperties = [{ name: "_ga", expireDays: 720 }];
const cookieOptions = {
  domain: "trakken.de",
  path: "/"
};

app.get("/", (req, res) => {
  cookieProperties.forEach(cookieProp => {
    const value = req.cookies[cookieProp.name];
    cookieOptions.expires = new Date(
      Date.now() + 1000 * 60 * 60 * 24 * cookieProp.expireDays
    );
    value && res.cookie(cookieProp.name, value, cookieOptions);
  });

  res.status(200).send();
});

app.post("/", (req, res) => {
  cookieProperties.forEach(cookieProp => {
    const value = req.cookies[cookieProp.name];
    cookieOptions.expires = new Date(
      Date.now() + 1000 * 60 * 60 * 24 * cookieProp.expireDays
    );
    value && res.cookie(cookieProp.name, value, cookieOptions);
  });

  res.status(200).send();
});

app.listen(port, () => console.log(`Cookie Saver listening on port ${port}!`));

module.exports = app; // for testing
