const chai = require("chai");
const chaiHttp = require("chai-http");

const app = require("../app");

chai.use(chaiHttp);

describe("app.js", () => {
  const cookieName = "_ga";
  const cookieValue = "GA1.1TESTGAID";
  it("saves my cookies with GET", done => {
    chai
      .request(app)
      .get("/")
      .set("Cookie", `${cookieName}=${cookieValue}`)
      .end((err, res) => {
        chai.expect(res).to.have.status(200);
        chai.expect(res).to.have.cookie(cookieName, cookieValue);
        done();
      });
  });
  it("saves my cookies with POST", done => {
    chai
      .request(app)
      .post("/")
      .set("Cookie", `${cookieName}=${cookieValue}`)
      .end((err, res) => {
        chai.expect(res).to.have.status(200);
        chai.expect(res).to.have.cookie(cookieName, cookieValue);
        done();
      });
  });
});
